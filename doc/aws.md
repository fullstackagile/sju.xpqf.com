# AWS configuration

## S3

### Bucket

- Name: sju.xpqf.com
- Region: eu-central-1
- Static Website Hosting: enabled
- Index document: index.html
- Policy:

```
{
  "Version": "2012-10-17",
  "Statement": [{
    "Sid": "PublicReadGetObject",
    "Effect": "Allow",
    "Principal": "*",
    "Action": "s3:GetObject",
    "Resource": "arn:aws:s3:::sju.xpqf.com/*"
  }]
}
```

## IAM

### Policy

- Name: s3-sju.xpqf.com-uploading

```
{
  "Version": "2012-10-17",
  "Statement": [{
    "Sid": "VisualEditor0",
    "Effect": "Allow",
    "Action": [
      "s3:GetObject",
      "s3:PutObject",
      "s3:DeleteObject"
    ],
    "Resource": "arn:aws:s3:::sju.xpqf.com/*"
  },{
    "Sid": "VisualEditor1",
    "Effect": "Allow",
    "Action": "s3:ListObjects",
    "Resource": "*"
  }]
}
```

### User

- Name: s3-sju.xpqf.com-uploader
- Access type: programmatic
- Policy: s3-sju.xpqf.com-uploading

## GitLab environment

- In Settings | CI/CD | Environment variables
- From IAM user account created above
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
