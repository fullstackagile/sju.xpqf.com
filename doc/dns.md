# DNS configuration

## DNS

for xpqf.com

- CNAME `sju` - `sju.xpqf.com.s3-website.eu-central-1.amazonaws.com`
- forward `www.xpqf.com/*` to `sju.xpqf.com/$1`
- forward `xpqf.com/*` to `sju.xpqf.com/$1`
- Always use HTTPS: `http://*.xpqf.com/*`
