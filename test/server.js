const httpTerminator = require('http-terminator');
const httpServer = require('http-server');

const { host, port } = require('./server.config.js');

let server;

before(async function () {
  this.timeout(10000);
  const root = 'public';
  server = httpServer.createServer({ root });
  await new Promise((resolve) => server.listen({ port, host }, resolve));
  console.log(`Serving directory "${root}" on ${host}:${port}`);
});

after(async function () {
  this.timeout(10000);
  await httpTerminator.createHttpTerminator(server).terminate();
});