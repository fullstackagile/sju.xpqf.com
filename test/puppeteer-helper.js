const fs = require('fs');

const clickToNavigate = async (selector, page) => {
  return Promise.all([
    page.waitForNavigation(),
    page.click(selector),
  ]);
};

const getAttribute = async (selector, attribute, page) => {
  return page.$eval(selector, (e, attribute) => e.getAttribute(attribute), attribute);
};

const getText = async (selector, page) => {
  return page.$eval(selector, e => e.textContent.trim());
};

const screenshot = async (name = 'page') => {
  fs.mkdirSync('output/screen', { recursive: true });
  await page.screenshot({ path: `output/screen/${name}.png` });
  const pageSource = await page.evaluate(() => document.querySelector('*').outerHTML);
  fs.writeFileSync(`output/screen/${name}.html`, pageSource);
};

module.exports = {
  clickToNavigate,
  getAttribute,
  getText,
  screenshot,
};