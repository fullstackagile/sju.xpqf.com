const fs = require('fs');

const { expect } = require('chai');
const puppeteer = require('puppeteer');

const { host, port } = require('./server.config.js');

const { clickToNavigate, getAttribute, getText, screenshot } = require('./puppeteer-helper.js');

// Note: selectors for finding elements on the page
const danishSelector = 'header ul li:nth-last-child(1) a';
const danishIconSelector = `${danishSelector} img`;
const englishSelector = 'header ul li:nth-last-child(2) a';
const homeLink = 'header a:nth-child(1)';
const pageTitle = 'h1';
const profileMenuItem = 'header ul li:nth-last-child(3) a';

describe('language', function () {
  let browser;
  let page;

  this.timeout(10000);

  before(async () => {
    const options = {};
    browser = await puppeteer.launch(options);
    const pages = await browser.pages();
    page = pages.pop();
    await page.goto(`http://${host}:${port}/`);
  });

  after(async () => {
    await browser.close();
  });

  it('should render our HTML', async () => {
    expect(await page.title()).to.equal('Sju G. Thorup');
    expect(await getText(pageTitle, page)).to.equal('Sju G. Thorup');
  });

  it('should start on English', async () => {
    expect(await getText(profileMenuItem, page)).to.equal('Profile');
  });

  it('should navigate to page within English', async () => {
    await clickToNavigate(profileMenuItem, page);
    expect(await getText(pageTitle, page)).to.equal('Profile')
  });

  it('should show tooltip on language selector', async () => {
    expect(await getAttribute(danishIconSelector, 'title', page)).to.equal('Dansk');
  });

  it('should navigate to same page in Danish', async () => {
    await clickToNavigate(danishSelector, page);
    expect(await getText(pageTitle, page)).to.equal('Profil')
  });

  it('should navigate to home within Danish', async () => {
    await clickToNavigate(homeLink, page);
    expect(await getText(pageTitle, page)).to.equal('Sju G. Thorup')
    expect(await getText(profileMenuItem, page)).to.equal('Profil');
  });

  it('should navigate to page within Danish', async () => {
    await clickToNavigate(profileMenuItem, page);
    expect(await getText(pageTitle, page)).to.equal('Profil')
  });

  it('should navigate to same page in English', async () => {
    await clickToNavigate(englishSelector, page);
    expect(await getText(pageTitle, page)).to.equal('Profile')
  });

  it('should navigate to home within English', async () => {
    await clickToNavigate(homeLink, page);
    expect(await getText(pageTitle, page)).to.equal('Sju G. Thorup')
    expect(await getText(profileMenuItem, page)).to.equal('Profile');
  });
});