module.exports = function (eleventyConfig) {
  eleventyConfig.addPassthroughCopy('src/style');
  eleventyConfig.setBrowserSyncConfig({
    open: 'local'
  });
  eleventyConfig.addFilter('log', value => {
    console.log('log', value)
  });
  return {
    templateFormats: [
      // Note: use liquid template engine
      'html',
      'md',
      // Note: uses passthroughFileCopy
      'gif',
      'jpg',
      'pdf',
      'png',
    ],
    dir: {
      input: 'src',
      layouts: "_layouts",
      output: 'public',
    },
    // passthroughFileCopy: true // TODO: not used anymore??
  };
};
