# sju.xpqf.com website

```bash
npm install
npm test
npm start
```

[Markdown reference](https://markdown-it.github.io/)

[Deployment setup](./doc/aws.md)

[DNS setup](./doc/dns.md)
