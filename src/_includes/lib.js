function saveLocale(locale) {
  localStorage.setItem('locale', locale);
}
function getLocale() {
  return localStorage.getItem('locale');
}