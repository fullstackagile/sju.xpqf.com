module.exports = {
  cvrLabel: {
    'da': 'CVR',
    'en': 'DK company registration no. (CVR)',
  },
  emailLabel: {
    'da': 'mail',
    'en': 'email',
  },
  phoneLabel: {
    'da': 'tlf',
    'en': 'tel. +45'
  }
};