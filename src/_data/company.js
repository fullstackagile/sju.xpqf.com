module.exports = {
  cvr: {
    'da': '36703121',
    'en': 'DK36703121',
  },
  address: {
    'da': 'Adolphsvej 66, Gentofte',
    'en': 'Adolphsvej 66, 2820 Gentofte, Denmark',
  }
};