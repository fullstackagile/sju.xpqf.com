---
layout: page
pagekey: home
permalink: /da/
tags: page
---

# Sju G. Thorup

<figure>

![](/media/Illustration-v002-vind-og-sol-forside-FINAL-20-procent-724x1024.jpg)

</figure>

Jeg tilbyder [konsulentbistand](./tilgang) til strategiske initiativer, der spænder på tværs af:

- teknisk nyudvikling
- organisatorisk forandring
- og forretningsmæssige forbedringer.

Med [ledersparring](./strategisparring) kan I sænke risici ved det strategiske initiativ. Med en sikker [projektledelse](./projektledelse) kan I øge projektets træfsikkerhed. Bistanden skal afhænge af jeres konkrete behov. I vil opleve, at jeg er lydhør overfor jeres specifikke situation, fokuseret på målene, og trækker på bred erfaring.

Se mit CV og professionelle [profil](./profil).

[![](/media/Sju-main-portrait-for-website-DSC1094-moderately-cropped-and-resized-217x300.jpg)](./profil/)

<figure>

![](/media/Illustration05-to-der-snakker-FINAL-cropped-20-procent-228x300.jpg)

</figure>
