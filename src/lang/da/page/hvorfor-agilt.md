---
layout: page
pagekey: why
permalink: /da/hvorfor-agilt/
tags: page
title: Hvorfor Agilt
---

# {{title}}

Den Agile tilgang er baseret på den erfaring, at ingen plan uændret overlever sit møde med virkelighedens verden.

![](/media/Plans_cartoon_smaller_JPG-768x777.jpg)
