---
layout: page
pagekey: approach
permalink: /da/tilgang/
tags: page
title: Tilgang
---

# {{title}}

<figure>

![](/media/Illustration_06_v003-mødebord-FINAL-25-procent-768x487.jpg)

</figure>

For at få et nyt initiativ til at lykkes, skal I have et fælles billede af, hvor I vil hen, og hvorfor I gør det. Derfor stiller jeg mange forskellige spørgsmål og lytter grundigt til jeres svar. Måske bliver det klart, at en vigtig del af organisationen skal inddrages mere, eller at en bestemt risiko skal afdækkes snarest muligt, eller at nogle planlagte aktiviteter kan nedprioriteres.

Jeg kommer ikke og spiller klog på jeres produkter, teknologi eller marked. Men jeg forstår hvad både teknikeren og lederen siger, og kan hjælpe med at skabe et fælles sprog. Sammen kan vi skabe letforståelige billeder af udfordringen, idéen eller visionen.

Kursus: Få en introduktion til det Agile felt, som sammenligner Agil med begreber fra traditionel projektledelse. [Læs mere](../introduktion-til-agilt-for-projektledere/).

Se [min tegneserie](../traditionel-projektledelse-moder-den-agile-tilgang/) som opsummerer den klassiske projektleders udfordringer med den Agile tilgang.

<iframe src="https://www.youtube.com/embed/LZWpfGjhq0w?feature=oembed" allow="autoplay; encrypted-media" allowfullscreen="" width="648" height="365" frameborder="0"></iframe>

Indlæg på BestBrains cafémøde, april 2016: “Hvor blev projektlederen af, da vi blev agile?”
