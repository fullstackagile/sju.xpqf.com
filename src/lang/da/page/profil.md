---
layout: page
pagekey: profile
permalink: /da/profil/
tags: page
title: Profil
---

# {{title}}

<figure class="callout">

<a href="https://www.amazon.com/Being-project-manager-Sju-Thorup/dp/1312838485" target="_blank">![](/media/Forside-til-websites-mv-Being-the-project-manager-292x300.jpg)</a>

<figcaption>

Find inspiration i min bog
“Being the Project Manager”

_Anmelderne sagde: Lettilgængelige projektledelses-råd til begyndere og erfarne – Fremragende letforståelig, klar formidling_

</figcaption>

</figure>

## Uddannelse

- PMI Agile Certified Practitioner, 2018
- Certified LeSS Practitioner, 2017
- SAFe Program Consultant, 2017
- Leading Corporate Entrepreneurship, Danish Technical University & Stanford University, 2008
- Executive MBA, International General Management, Copenhagen Business School, 2000
- M.Sc., Computer Science, University of Copenhagen, 1993

## Erhverv

Selvstændig managementkonsulent, 2015 –

Forfatter, 2014 – se [tegneserie](../traditionel-projektledelse-moder-den-agile-tilgang) fra bogen

Selvstændig managementkonsulent, 2012-2014

Senior projektleder, Silver Spring Networks, 2011

Progress manager, DONG Energy R&D, 2007-2010

Projektleder, Dansk Standard, 2006

Selvstændig managementkonsulent, 2005

Managementkonsulent, PA Consulting Group, 2001-2004

VP R&D Coordination, Adomo, 2000

Softwareudvikler/ udviklingschef, Scan-Jour, 1996-2000

_Se CV på <a href="https://dk.linkedin.com/in/sjuthorup" target="_blank">Linkedin</a>._

![](/media/Sju-elproduktion-kontrolrum-Skærbæk-slightly-cropped-256x300.jpg)
![](/media/Sju-smart-grid-project-wind-turbine-and-electric-cars-1024x1010-300x296.jpg)
![](/media/Sju-på-Avedøreværket-slightly-cropped-236x300.jpg)
