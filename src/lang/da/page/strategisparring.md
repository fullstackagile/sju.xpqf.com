---
layout: page
pagekey: strategy
permalink: /da/strategisparring/
tags: page
title: Strategisk Sparring
---

# {{title}}

<figure>

![](/media/Illustration_no3-ledersparring-FINAL-cropped-og-20-procent-271x300.jpg)

</figure>

Når man som leder skal stå i spidsen for et nyt initiativ, kan man have nytte af sparring med en neutral part. En person som hverken bliver påvirket af forandringerne eller skal bedømme om det blev en succes.

En sparringssession med jævne mellemrum kan hjælpe med at holde fokus og give input til prioritering og næste skridt. Jeg kan ikke fortælle dig, hvordan du skal drive din forretning, men stiller spørgsmål som støtter dine indsigter og beslutninger.

## Erfaring

**Udviklingsvirksomhed** — hi-tech produktudvikling (2019): Coaching af projektledere, etablering af løbende forbedringsprocesser.

**Finansiel** virksomhed — IT (2018): Bistand til opstart af en SAFe transformation.

**Universitet** — den centrale IT-funktion (2018): Coaching af teams i Agile arbejdsformer.

**Konsulenthus** — organisationsudvikling (2017): Sparring med ledelsen om vækstinitiativer.

**Erhvervsstyrelsen** — offentlig administration (2016): Sparring med projektleder om strategisk projektindstilling.

**Konsulenthus** — specialydelse indenfor socialsektoren (2016): Sparring med ledelsen om vækstinitiativer.

**Broth.dk** — fødevarer, B2C og B2B (2015-2016): Sparring med stifteren om forretningsmodel og forretningsudvikling.

**Autoimmune Therapies** — healthcare/ life science, B2C (2011-2012): Sparring med stifteren om forretningsmodel og forretningsudvikling.

**Cleantech Open** — vedvarende energi, B2B (2011): Mentor for to deltagere i California Cleantech Open. Sparring om forretningsmodel og markedsstrategi.

**ZeaLake** — konsulentvirksomhed, B2B (2010-2015): Sparringspartner i nyetablering i nyt geografisk marked.

**Gaia Solar** — vedvarende energi, B2B (2005): Strategisk forretningsudvikling.

**Mutual Satellites** — software, B2B (2006): Sparring med stifteren, assistance til patent og markedsstrategi.

**BestBrains** — konsulentvirksomhed, B2B (2001-2005): Sparring med stifteren fra idé-stadiet til de første ansættelser.

## Kunderne siger

> “At have Sju som sparringspartner gør, at jeg får mere struktur på mine initiativer og handlingsplaner. Hun guider og kommer med forslag til, hvordan tingene bedst kan gøres, men er aldrig påtrængende eller belærende, hvilket gør at det virkelig er mine idéer og min måde at gøre tingene på hun får sat i struktur. Sju har også et hav af metoder, som hun med stor dygtighed gør tydelige og relevante for min virksomhed. Sju motiverer mig, holder mig til ilden, hjælper mig med konkrete ting, får uoverskueligt mange gøremål kogt ned til de tre vigtigste, og så er hun god til at holde overblikket.”

“Som min coach har Sju igen og igen demonstreret sin høje intelligens, evne til at forstå kompleksiteten af vores forretningsområde og min situation, og at give mig feedback og handlingsforslag, som direkte addresserer det vigtigste. Som min rådgiver i dette mit seneste og mest ambitiøse startup har hun udfordret mig og hjulpet mig med at vokse som leder, på måder som jeg ikke troede muligt — og så er hun herligt fri for konsulent-speak.”
