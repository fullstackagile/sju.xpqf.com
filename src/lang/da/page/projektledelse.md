---
layout: page
pagekey: project
permalink: /da/projektledelse/
tags: page
title: Projektlederskab
---

# {{title}}

<figure>

![](/media/Illustration_no2-projektledelse-succes-FINAL-20-procent-768x403.jpg)

</figure>

Projekter som har mange interessenter og risici kræver en sikker hånd. I kan øge projektets træfsikkerhed med en projektleder, som har flair for mennesker, forståelse for tekniske problemstillinger, sans for forretningen og stort overblik.

Projektledelse i klassisk forstand er nyttigt, når man skal bygge en bro, men sjældent når man skal udvikle en ny produktidé eller forandre organisationen. Derfor anbefaler jeg en mere agil tilgang. Der er brug for et landkort med de store linjer, men ikke en detaljeret plan som alligevel brister ved sit første møde med virkeligheden. Til gengæld skal projektet kunne prioritere eksplicit og synliggøre sin fremdrift.

Denne tegneserie illustrerer, hvorfor en Agil tilgang tit er nyttig og realistisk: [Ofte stemmer planer og virkelighed ikke overens](../hvorfor-agilt). Og denne viser de udfordringer, som en klassisk projektleder kan opleve i mødet med en Agil tilgang: [Traditionel projektledelse møder den Agile tilgang](../traditionel-projektledelse-moder-den-agile-tilgang/). Se også kursusmuligheden “[Agilt for projektledere](../introduktion-til-agilt-for-projektledere/)“.

## Erfaring

**Planlægningsværktøj**, energiselskab — IT (2017): Agil projektleder for udvikling af et støtteværktøj til planlægning af vindmølleparker.

**Salgssystem**, energiselskab — IT, B2B (2015-2016): Projektleder for kundens product owner team, leverandørstyring og organisatorisk implementering.

**Smartgrid**, Silver Spring Networks — software, B2B (2011): Projektleder og scrum master.

**Power Hub**, DONG Energy — vedvarende energi, B2B (2009-2010): Projektleder og product manager for et banebrydende smartgrid-koncept.

**R&D-portefølje**, DONG Energy — energiteknologier, B2B (2007-2009): Corporate innovation, business cases og koordinering af projektporteføljen.

**Smiley på internettet**, Fødevarestyrelsen — IT, offentlig administration (2003): Teknisk og procesmæssig assistance til projektlederen.

**TingDok**, Folketinget— IT, offentlig administration (2001-2004): Teknisk og procesmæssig assistance til projektlederen fra idé til implementering.

**Portefølje af softwareprodukter**, Scan-Jour (1997-2000) — Portføljestyring af produktudviklingen i en softwareudviklingsvirksomhed.

**SJ Intranet**, Scan-Jour (1999) — Første browser-baserede udgave af dokumenthåndteringssystem.

## Kunderne siger

> “Sju er en visionær og inspirerende leder, som kan få et team til at yde sit bedste. Hun motiverede vores projektgruppe til at skabe innovation indenfor teknologier, metoder, organisation, processer og regulatoriske rammer. Sju besidder en lang række både ‘bløde’ og ‘hårde’ kompetencer, og jeg var imponeret over hendes evne til at fastholde det langsigtede mål, mens de daglige betingelser og prioriteter skiftede.”

> “Sju er en struktureret projektleder med en stærk analytisk sans, solidt overblik og evne til at sætte sig ind i komplekse problemstillinger. Hun er også en dygtig kommunikator og god til at samarbejde på tværs af hele organisationen.”
