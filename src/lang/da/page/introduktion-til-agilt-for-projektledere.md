---
layout: page
pagekey: course
permalink: /da/introduktion-til-agilt-for-projektledere/
tags: page
title: "Kursus: Introduktion til Agilt for projektledere"
---

# {{title}}

<figure>

![](/media/Authority-cartoon-for-Agile-use-frame-3-for-thumbnail-smaller-1-May-2018-Sju-300x300.jpg)

</figure>

Få en introduktion til det Agile felt, som sammenligner Agil tilgang med begreber fra traditionel projektledelse. Du lærer de agile grundprincipper at kende og kommer til at se dem i lyset af de klassiske projektbegreber.

Kurset sammenligner begreber fra den klassiske projektopfattelse med de tilsvarende begreber fra den Agile forståelse. Der er som regel ikke en direkte overensstemmelse, og det giver udfordringer både for den enkelte og for organisationen. Vi giver overblik over de Agile principper, metoder, og forudsætningerne for at et Agilt tiltag kan lykkes. Et kig på personlighedstyper giver deltagerne inspiration til at søge deres nye Agile roller og kontekster.

Kurset er et 1-dagskursus der indeholder både undervisning, øvelser og gruppediskussioner om Agile projekter fra det virkelige liv. Lad os tale om denne mulighed for at skabe et fælles udgangspunkt for jeres projektleder-gruppes Agile rejse!

Se ogå [en tegneserie](../traditionel-projektledelse-moder-den-agile-tilgang) som opsummerer den klassiske projektleders udfordringer med den Agile tilgang. Tegneserien er fra <a href="https://www.amazon.com/Being-project-manager-Sju-Thorup/dp/1312838485" target="_blank">min bog</a> , som kan være inspirerende hvis du er relativt ny til projektledelse.
