---
layout: page
pagekey: strategy
permalink: /en/strategic-coaching/
tags: page
title: Strategic Coaching
---

# {{title}}

<figure>

![](/media/Illustration_no3-ledersparring-FINAL-cropped-og-20-procent-271x300.jpg)

</figure>

As a leader heading a new initiative, you can benefit from an insightful conversation with a neutral party. A person who will neither be subject to the anticipated change nor will be in any position to judge you for the results.

Regular or occasional coaching sessions can help you focus and provide input to your setting of priorities and deciding next steps. I can’t tell you how to conduct your business, but I ask questions and provide suggestions that will support your reflection and decision-making.

## Experience

**Hi-tech company** — product development (2019): Coaching project managers, instituting continuous improvement processes.

**Financial company** — IT (2018): Assisting the initiation of a SAFe transformation.

**University** — central IT (2018): Coaching teams in taking up Agile ways of working.

**Consulting firm** — organizational development (2017): Coaching the founders in leadership for growth.

**Danish Business Authority** — public sector (2016): Coaching a project manager writing up a strategic investment proposal.

**Specialist consulting firm** — service to the social sector (2016): Leadership coaching focused on growth.

**Broth.dk** — food sector, B2C and B2B (2015-2016): Coaching the founder with a focus on business model and business development.

**Autoimmune Therapies** — healthcare/ life science, B2C (2011-2012): Coaching the founder with a focus on business model and business development.

**Cleantech Open** — renewable energy, B2B (2011): Mentor to two participants in the California Cleantech Open competition. Business models and go-to-market strategies.

**ZeaLake** — software consulting firm, B2B (2010-2015): Co-pilot in new market entry.

**Gaia Solar** — renewable energy, B2B (2005): Strategic business development.

**Mutual Satellites** — software product, B2B (2006): Coaching the founder, assistance to patent application and go-to-market strategy.

**BestBrains** — consulting firm, B2B (2001-2005): Coaching the founder from idea to the initial recruitments.

## Client quotes

> “Having Sju as my business coach helps me structure my initiatives and plans. In our conversations she will guide me and suggest approaches, but never in a patronizing or imposing way. Sju also knows an abundance of methods that she expertly brings into play as relevant. She motivates me, keeps me on my toes, helps me solve specific problems, boils down my interminable to-do list to the three most important actions, while she maintains the big picture.”

> “In her role as my coach, Sju has proven again and again her extraordinary intelligence, ability to grasp the complexities of our business and my situation rapidly, and to provide feedback and actionable items that cut straight to what is important. As advisor to me in my latest and most ambitious start-up she has challenged me and helped me grow as a leader and manager, in ways I did not think possible, and she is gloriously jargon-free.”
