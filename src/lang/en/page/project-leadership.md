---
layout: page
pagekey: project
permalink: /en/project-leadership/
tags: page
title: Project Leadership
---

# {{title}}

<figure>

![](/media/Illustration_no2-projektledelse-succes-FINAL-20-procent-768x403.jpg)

</figure>

When a project involves many stakeholders and risks, it needs a pair of safe hands. The prospect of success can be greatly enhanced by engaging a project leader who integrates the strategy, gets the technology, understands people, and grasps the complexity of the situation.

Classic project management is helpful when building a bridge but not so much when developing a new technical idea or transforming the organization. I recommend a more agile approach. A roadmap is necessary to convey the direction, but any detailed plan would break up anyway at its first encounter with real life. What the project needs is a way of prioritizing clearly and communicating progress.

This cartoon illustrates why an Agile approach is often a fruitful and realistic approach: [Often plans don’t reflect reality](../why-agile/). And this one shows the challenges that a classic project manager may have when encountering an Agile setting: [Traditional project management meets the Agile approach](../traditional-project-management-meets-the-agile-approach/). Also check out the training course “[Agile for project managers](../introduction-to-agile-for-project-managers/)“.

## Experience

**Project planning tool**, energy corporation — IT (2017): Agile project manager for development of a support tool for windfarm planning.

**Sales system**, energy corporation — IT, B2B (2015-2016): Project manager for the client’s product owner team, supplier management and organizational implementation.

**Smartgrid**, Silver Spring Networks — software, B2B (2011): Project manager and scrum master.

**Power Hub**, DONG Energy — renewable energy, B2B (2009-2010): Project manager and product manager for a leading-edge smartgrid concept.

**R&D project portfolio**, DONG Energy — energy technologies, B2B (2007-2009): Corporate innovation, business cases and coordination of the project portfolio.

**Smiley on the internet**, Danish Food Administration — IT, public sector (2003): Technical and process assistance to the project manager.

**TingDok**, The Danish Parliament Administration — IT, public sector (2001-2004): Technical and process assistance to the project manager from idea to implementation.

**Portfolio of software products**, Scan-Jour — Software product, B2B (1997-2000): Portfolio management of the product development of a software company.

**SJ Intranet**, Scan-Jour — Software product, B2B (1999): First browser-based version of a document management system.

## Client quotes

> “Sju is a visionary and inspiring leader that can make a group excel. She motivated our team to innovate within technologies, methods, organizational processes and regulatory matters. Sju is a resourceful person with a wide range of both ‘hard’ and ‘soft’ skills, and I was impressed by her ability to maintain long-term priorities while continuously adjusting short-term focus and actions.”

> “Sju is a structured project manager with a strong analytical sense, a reliable overview and good grasp of complicated situations. She is also a great communicator and was a highly appreciated collaborator throughout the organization.”
