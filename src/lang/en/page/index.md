---
layout: page
pagekey: home
permalink: /en/
tags: page
---

# Sju G. Thorup

<figure>

![](/media/Illustration-v002-vind-og-sol-forside-FINAL-20-procent-724x1024.jpg)

</figure>

I offer [consulting services](./approach) for strategic initiatives that include:

- technology development
- organizational change
- and business growth.

Insightful [strategic coaching](./strategic-coaching/) can lower the risks of your business initiative. Firm [project leadership](./project-leadership/) can increase your return on the project investment. My assistance is focused on your long-term goals, specific to your needs, and based on wide experience.

See my resumé and professional [profile](./profile/).

[![](/media/Sju-main-portrait-for-website-DSC1094-moderately-cropped-and-resized-217x300.jpg)](./profile/)

<figure>

![](/media/Illustration05-to-der-snakker-FINAL-cropped-20-procent-228x300.jpg)

</figure>
