---
layout: page
pagekey: profile
permalink: /en/profile/
tags: page
title: Profile
---

# {{title}}

<figure class="callout">

<a href="https://www.amazon.com/Being-project-manager-Sju-Thorup/dp/1312838485" target="_blank">![](/media/Forside-til-websites-mv-Being-the-project-manager-292x300.jpg)</a>

<figcaption>

Find inspiration in my book “Being the Project Manager”

_Reviewers said: Very accessible Project Management advice for beginner and experienced PMs. – Amazing accessible, clear approach._

</figcaption>

</figure>

## Education

- PMI Agile Certified Practitioner, 2018
- Certified LeSS Practitioner, 2017
- SAFe Program Consultant, 2017
- Leading Corporate Entrepreneurship, Danish Technical University & Stanford University, 2008
- Executive MBA, International General Management, Copenhagen Business School, 2000
- M.Sc., Computer Science, University of Copenhagen, 1993

## Professional experience

Independent management consultant, 2015 –

Author, 2014 – See a [cartoon](../traditional-project-management-meets-the-agile-approach/) from the book

Independent management consultant, 2012-2014

Senior project manager, Silver Spring Networks, 2011

Progress manager, DONG Energy R&D, 2007-2010

Project manager, Dansk Standard, 2006

Independent management consultant, 2005

Management consultant, PA Consulting Group, 2001-2004

VP R&D Coordination, Adomo, 2000

Software developer/ VP engineering, Scan-Jour, 1996-2000

_See my resumé at <a href="https://dk.linkedin.com/in/sjuthorup" target="_blank">Linkedin</a>._

![](/media/Sju-elproduktion-kontrolrum-Skærbæk-slightly-cropped-256x300.jpg)
![](/media/Sju-smart-grid-project-wind-turbine-and-electric-cars-1024x1010-300x296.jpg)
![](/media/Sju-på-Avedøreværket-slightly-cropped-236x300.jpg)
