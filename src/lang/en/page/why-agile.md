---
layout: page
pagekey: why
permalink: /en/why-agile/
tags: page
title: Why Agile
---

# {{title}}

The origin of the Agile approach is the fact that no plan survives unchanged by its encounter with reality.

![](/media/Plans_cartoon_smaller_JPG-768x777.jpg)
