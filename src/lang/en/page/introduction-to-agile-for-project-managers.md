---
layout: page
pagekey: course
permalink: /en/introduction-to-agile-for-project-managers/
tags: page
title: "Training course: Introduction to Agile for project managers"
---

# {{title}}

<figure>

![](/media/Authority-cartoon-for-Agile-use-frame-3-for-thumbnail-smaller-1-May-2018-Sju-300x300.jpg)

</figure>

Get an introduction to the Agile concepts, contrasting them with traditional project management methods. This course links classic project management to the Agile approach and shows ways to transition between them.

We map terms and concepts in the classic project management models to the corresponding ones in Agile. Often there is no one-to-one mapping, and this can cause challenges for a classic project manager and their organisation. We discuss the preconditions for using an Agile approach and cover the typical new Agile roles and their responsibilities. In the Agile approach the role of Project Manager does not exist, but there still is a need for similar competencies. We will discuss which Agile roles take over the various responsibilities of a classic project manager, and where the best fit may be for you.

This 1-day training course includes teaching sessions, a number of engaging exercises, and discussions about real-world Agile projects. Get in touch if you want to discuss this great way for your project management team to kick off your Agile journey!

Also check out [my cartoon](./../traditional-project-management-meets-the-agile-approach/) that sums up the challenges of a classic project manager encountering the Agile approach. The cartoon is from <a href="https://www.amazon.com/Being-project-manager-Sju-Thorup/dp/1312838485" target="_blank">my book</a> which can be of inspiration if you are a relative newcomer to project management.
