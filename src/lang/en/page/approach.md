---
layout: page
pagekey: approach
permalink: /en/approach/
tags: page
title: Approach
---

# {{title}}

<figure>

![](/media/Illustration_06_v003-mødebord-FINAL-25-procent-768x487.jpg)

</figure>

For your new initiative to succeed, your team needs a shared understanding of the direction and the objective. In our conversations I will ask a lot of questions and listen carefully to your answers. It may emerge that a crucial part of the organization must be more involved, or a specific risk should be mitigated as soon as possible, or that some planned activities can be postponed.

I am not an expert in your products, technology or market. But I understand the engineer as well as the business person, and I can support you in establishing a shared language. Together we will create an effective way to communicate the challenge, the idea, or the vision.

Training course: Get an introduction to the Agile concepts, contrasting them with traditional project management methods. [Read more](../introduction-to-agile-for-project-managers/).

Check out [my cartoon](../traditional-project-management-meets-the-agile-approach/) that summarizes the classic project manager’s challenges with the Agile approach.

<iframe src="https://www.youtube.com/embed/LZWpfGjhq0w?feature=oembed" allow="autoplay; encrypted-media" allowfullscreen="" width="648" height="365" frameborder="0"></iframe>

Indlæg på BestBrains cafémøde, april 2016: “Hvor blev projektlederen af, da vi blev agile?”
